/**
  @file index.js
  @date 2020-3
  @author Génesis García Morilla <gengns@gmail.com>
  @description Main
*/

// Slides initialization
Reveal.initialize({
  display: 'flex',
  width: '100%',
  height: '100%',
  margin: 0,
  minScale: 1,
  maxScale: 1
})


// Slides interactions
function set_interactions() {
  // If trying to use the device browser set Full Screen
  const $full = document.querySelector('#fullscreen')
  const $controls = document.querySelector('.reveal .controls')
  if ('ontouchstart' in window && typeof cordova === 'undefined') {
    $full.classList.remove('hide')
    $controls.style.zIndex = '-11'
    $full.onclick = () => {
      document.body.requestFullscreen()
      $full.classList.add('hide')
      $controls.style.zIndex = '11'
    }
  }

  // Translate
  const str = localStorage['language'] || 'es' // default language
  if (str == 'es') set_language(str)
  else {
    fetch(`js/config/language.${str}.json`)
      .then(response => response.json())
      .then(obj => set_language(str, obj))
      .catch(error => console.error(error, str))
  }

  document.querySelector('select').onchange = e => {
    localStorage['language'] = e.target.value
    location.reload()
  }


  // Generic help (you can touch or drag with your finder)
  const $touch = document.querySelector('#touch')
  const $drag = document.querySelector('#drag')

  // Move PIMO Games logo every 5s
  const $pimo = document.querySelector('img[src*=screen]')
  setInterval(() => animate($pimo, 'rubberBand'), 5e3)

  const $h1 = document.querySelector('section:first-of-type h1')
  const $manuela = document.querySelector('section:first-of-type h2')
  const $ikson = document.querySelector('section:first-of-type h2:nth-of-type(2)')
  const $select = document.querySelector('select')

  $pimo.classList.remove('hide')
  animate($pimo, 'bounceInDown')
  $h1.classList.remove('hide')
  animate($h1, 'bounceInUp', () => {
    $manuela.classList.remove('hide')
    animate($manuela, 'bounceInLeft', () => {
      $ikson.classList.remove('hide')
      animate($ikson, 'bounceInRight', () => {
        $select.classList.remove('hide')
        animate($select, 'bounceInUp')
      })
    })
  })

  // Click on Ikson text to stop/start song
  const $audio = document.querySelector('audio')
  $ikson.onclick = () => {
    $audio.paused ? $audio.play() : $audio.pause()
  }

  // Jump when clicked
  const $openmouth = document.querySelector('img[src*=openmouth]')

  $openmouth.onclick = () => {
    $touch.classList.add('hide')
    animate($openmouth, 'bounce')
  }


  // Draw random sized Covids
  const canvas_travel = document.querySelector('canvas#travel')
  const ctx_travel = canvas_travel.getContext('2d')

  set_canvas(canvas_travel, ctx_travel)
  draw_image_on_click(canvas_travel, ctx_travel, $openmouth)
  window.addEventListener('resize', () => set_canvas(canvas_travel, ctx_travel))


  // Move hand every 5s
  setInterval(() => animate('img[src*=hand]', 'wobble'), 5e3)


  // Faces
  const animation = ['bounceOutLeft', 'rotateOut', 'fadeOutUp',
    'flip', 'hinge', 'zoomOutDown']
  document.querySelectorAll('figure').forEach(($figure, i) =>
    $figure.onclick = () => animate($figure, animation[i]))


  // Draw how you feel
  const canvas_feel = document.querySelector('canvas#feel')
  const ctx_feel = canvas_feel.getContext('2d')

  set_canvas(canvas_feel, ctx_feel)
  set_freehand_drawing(canvas_feel, ctx_feel, 2)
  window.addEventListener('resize', () => set_canvas(canvas_feel, ctx_feel))


  // Lift curtain
  const $section_tv = document.querySelector('section#tv')
  const $curtain = document.querySelector('img[src*=curtain]')

  if ('ondrag' in window && 'onpointerup' in window)
    set_vertical_screen_drag($section_tv, $curtain, 50, () => {
      $drag.classList.add('hide')
    })
  else {
    $drag.classList.add('hide')
    $curtain.onclick = () => $curtain.classList.add('hide')
  }

  window.addEventListener('resize', () => $curtain.style.top = 0)


  // Animation at school
  let $blackboard = document.querySelector('img[src*=blackboard]')
  let $teacher = document.querySelector('img[src*=teacher]')
  let $ruler = document.querySelector('img[src*=ruler]')
  $blackboard.onclick = () => animate($blackboard, 'rubberBand')
  $teacher.onclick = () => animate($teacher, 'tada')
  $ruler.onclick = () => animate($ruler, 'slideOutRight')


  // Sympton
  const $symptom = document.querySelector('img[src*=symptom]')
  $symptom.onclick = () => animate($symptom, 'flash')


  // Bye bye
  const $lungs = document.querySelector('img[src*=lungs]')
  $lungs.onclick = () => animate($lungs, 'rubberBand')
  const $bye = document.querySelector('img[src*=bye]')
  $bye.onclick = () => animate($bye, 'bounceOutDown')


  // House
  const $house = document.querySelector('img[src*=house]')
  $house.onclick = () => animate($house, 'tada')


  // I can help
  const $tap = document.querySelector('img[src*=tap]')
  $tap.onclick = () => animate($tap, 'rubberBand')
  const $cake = document.querySelector('img[src*=cake]')
  $cake.onclick = () => animate($cake, 'rubberBand')

  // Doctors
  const $lover = document.querySelector('img[src*=lover]')
  $lover.onclick = () => animate($lover, 'fadeOutLeftBig')
  const $idcard = document.querySelector('img[src*=idcard]')
  $idcard.onclick = () => animate($idcard, 'fadeOutRightBig')
}



// Check if not deployed with Cordova
if (typeof cordova === 'undefined') set_interactions()
else document.addEventListener('deviceready', () => set_interactions())