/**
  @file utils.js
  @date 2020-3
  @author Génesis García Morilla <gengns@gmail.com>
  @description Utils
*/

let T_ // Translator


/**
 * Select language and set up translator
 * @param {string} str Language 'en', 'ru', etc
 * @param {object|undefined} T Translator
 */
function set_language(str, T) {
  // Set language in the HTML select
  document.querySelector('select').value = str
  T_ = txt => T && T[txt] ? T[txt] : txt
  // Store language configuration
  localStorage['language'] = str
  localStorage['T'] = JSON.stringify(T || '')
  // Translate
  document.querySelectorAll('.T').forEach(e =>
    e.textContent = T_(e.textContent))
}


// Animate
function animate(e, animationName, callback) {
  const node = typeof e === 'string' ? document.querySelector(e) : e
  node.classList.add('animated', animationName)

  function handleAnimationEnd() {
    node.classList.remove('animated', animationName)
    node.removeEventListener('animationend', handleAnimationEnd)

    if (typeof callback === 'function') callback()
  }

  node.addEventListener('animationend', handleAnimationEnd)
}


// Get random number
const get_random = (min, max) => Math.random() * (max - min) + min


// Clean and set canvas dimensions
function set_canvas(canvas, ctx) {
  // Clear
  ctx.clearRect(0, 0, canvas.width, canvas.height)
  // Same window dimensions
  canvas.width = window.innerWidth
  canvas.height = window.innerHeight
}


// Draw image in canvas on click
function draw_image_on_click(canvas, ctx, $img) {
  const start_ = e => {
    const { pageX: x, pageY: y } = e
    // Random proportions
    const s = get_random($img.width / 20, $img.width / 2)
    // Draw in the center
    ctx.drawImage($img, x - s / 2, y - s / 2, s, s)
  }

  if ('onpointerdown' in window) canvas.onpointerdown = e => start_(e)
  else canvas.ontouchstart = e => start_(e)
}


// Set freehand drawing on canvas
function set_freehand_drawing(canvas, ctx, line_width) {
  // We need to track mouse/finger position and down/up
  let x, y, down // we could set this variables inside the canvas obj (as you wish)

  // Start
  const start_ = e => {
    const { pageX, pageY } = e
    down = true
    x = pageX
    y = pageY
  }

  if ('onpointerdown' in window) canvas.onpointerdown = e => start_(e)
  else canvas.ontouchstart = e => start_(e)

  // End
  const end_ = () => down = false

  if ('onpointerup' in window) canvas.onpointerup = end_
  else canvas.ontouchend = end_

  // Move
  const move_ = e => {
    // Avoid extra events (reveal.js)
    e.stopPropagation()
    // Return if we haven't finish yet
    if (!down) return
    const { pageX, pageY } = (e.touches && e.touches[0]) || e
    // Draw line
    ctx.beginPath()
    ctx.moveTo(x, y)
    ctx.lineTo(pageX, pageY)
    ctx.lineWidth = line_width
    ctx.stroke()
    // Update
    x = pageX
    y = pageY
  }

  if ('onpointermove' in window) canvas.onpointermove = e => move_(e)
  else canvas.ontouchmove = e => move_(e)
}


// Set vertical drag with screen constraints
function set_vertical_screen_drag($container, $e, top_limit, callback) {
  // Avoid default drag behavior
  $e.ondrag = $e.ondragstart = e => e.preventDefault()
  // New drag behavior
  $e.onpointerdown = startDrag
  if ('onpointerdown' in window) $e.onpointerdown = startDrag
  else $e.ontouchstart = startDrag

  function startDrag() {
    if ('onpointerup' in window) $container.onpointerup = finishDrag
    else $container.ontouchend = finishDrag

    $container.onpointermove = e => {
      const y = $e.height + parseInt($e.style.top) + e.movementY
      // Top limit
      if (y < top_limit && e.movementY < 0) return
      // Bottom limit
      if (y > $e.height && e.movementY > 0) return
      // New position
      $e.style.top = `${$e.offsetTop + e.movementY}px`
    }

    if (callback) callback()
  }

  function finishDrag() {
    $container.onpointerup = null;
    $container.onpointermove = $container.ontouchmove = null;
  }
}

