# What is Hello Covid?

**Hello Covid** is an interactive story, built to support and reassure children under the age of 7, regarding the COVID-19.

This content is a resource for families and educators to allow discussions and conversations with kids regarding the current situation and how they feel about it.

It also try to tell the children who the coronavirus is and how we can deal with it to avoid anxiety and frustration.

It is important to point out that this resource does not seek to be a source of scientific information.

It is a free and public resource but you must credit the author: PIMO Games

## Web
Read the interactive story online: [Hello Covid on Gitlab](https://pimogames.gitlab.io/hello-covid/)

## App
Download the app on your device: [Hello Covid for Android](https://gitlab.com/pimogames/hello-covid/-/raw/master/hello-covid-v1.0.0.apk)

## Contact
Any comment, doubt or suggestion drop us an email: pimogames@gmail.com

❤ Happy to hear from you!

## Credits
Story based on Manuela Molina's book, available on [Mindheart.co]( https://www.mindheart.co/descargables)

Song "Walk" by [ikson](http://www.soundcloud.com/ikson)

Music promoted by [Audio Library](https://youtu.be/szEfp07r5Cg)
